# Requisitos

Debe tener los siguientes paquetes instalados.
- Php version 8.1
- Composer version 2.5.8
- Laravel Installer 4.5.0
- packages: php-dom, php-curl
- editar archivo php.ini y descomentar la linea "extension=curl"

# Ejecucion

Antes de iniciar la aplicacion, debe instalar las dependencias de Composer con el siguiente comando:

```bash
composer install

## si presenta problemas, debe ejecutarlo con el siguiente tag

composer install --ignore-platform-req=ext-xml

 ## si los archivos ya se encuentran presentes debe ejecutar el siguiente

composer update --ignore-platform-req=ext-xml
```

Una vez instaladas, se debe ejecutar mediante el siguiente comando:

```bash
php artisan serve
```